import './style/style.css'
import { getSentence, setupSentence } from './src/sentences.js'
import { setupValidator } from './src/validator.js'
import { DoublyLinkedList } from './src/model/linked_list'


// Adding container
document.querySelector('#app').innerHTML = `
  <div class="text-container">
    <div class="text-inside"></div>
  </div>
`

setupSentence(document.querySelector(".text-inside"))
setupValidator();