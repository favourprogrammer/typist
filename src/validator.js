import { resizeWord, getSentence, transformSentence } from "./sentences.js"

function setupValidator() {
    let typed = "", sentence = getSentence();
    function singlePress(element) {
        if (element.key.length === 1 && typed.length < sentence.count) {
            const el = sentence.getElementAt(typed.length).value.node;

            if ( el.textContent === element.key) {
                el.className = 'right';
            } else {
                el.className = 'wrong';
            }
            typed += element.key;
        }
        toggleCursor();
        transformSentence();
    }

    function toggleCursor() {
        const csr = sentence.find(el => el.node.classList.contains('cursor'));
        const nxt = sentence.getElementAt(typed.length);

        if (csr) csr.value.node.classList.remove('cursor');
        if (typed.length < sentence.count) nxt.value.node.classList.add('cursor');
    }

    function backspacePress(element) {
        if (element.key === "Backspace" && typed.length < sentence.count) {
            if (element.ctrlKey) {
                const words = resizeWord(typed);
                const wd = sentence.filter(e => e.index == words.length - 1);

                let curr = wd.head
                for (let i = 0; i < wd.count; i++) {
                    curr.value.node.className = "";
                    curr = curr.next;                    
                }

                
                typed = words.slice(0, words.length - 1).join("");
            } else {
                const el = sentence.getElementAt(typed.length).value.node;
                el.className = "";
                typed = typed.substring(0, typed.length - 1);
            }
        }
        toggleCursor()
        transformSentence();
    }

    document.addEventListener('keypress', singlePress)
    document.addEventListener('keydown', backspacePress)
    window.addEventListener('resize', transformSentence)
    toggleCursor()
    transformSentence();
}

export { setupValidator };