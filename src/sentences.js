import json from "./json/quote.json"
import { DoublyLinkedList } from "./model/linked_list";


/** Return a random integer value between the specified minimum and maimum values. */
function randInt(min, max) {
    return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min)) + Math.ceil(min));
}


/** Converts the specified text string into a paragraph dom string. */
export function textToParagraph(text) {
    return `<p>${[...text].reduce((a, b) => a + `<span>${b}</span>`, "")}</p>`;
}


/** Splits the specified sentence into an array of words. */
export function sentenceToWord(sentence) { return sentence.split(/(?<=\s)/); }


/** Return the rendered in the text playground's dom nodes. */
export function getSentence() {
    const elements = document.querySelectorAll(".text-inside p");
    const sentence = new DoublyLinkedList();

    for (let index = 0; index < elements.length; index++) {
        elements[index].childNodes.forEach(node => sentence.push({ index, node }))
    }
    return sentence;
}


/** Splits the specified text into into an array that's the same size as sentence words. */
export function resizeWord(text) {
    const words = sentenceToWord(document.querySelector(".text-inside").textContent);
    const result = [];

    for (let i = 0; i < words.length && text; i++) {
        result.push(text.substr(0, words[i].length))
        text = text.substr(words[i].length);
    }
    return result;
}


/** Vertically displaces the rendered sentence when the user enters a newline. */
export function transformSentence() {
    const cursor = document.querySelector(".cursor");
    if (cursor) {
        const element = document.querySelector(".text-inside");

        const cursorPos = cursor.parentNode.getBoundingClientRect().y;
        const wdPos = element.childNodes[0].getBoundingClientRect().y;

        element.style.transform = `translateY(${Math.round(cursorPos - wdPos) * -1}px)`;
    }
}


/** Renders the text playground inside the provided dom element. */
export function setupSentence(element) {
    let sentence;

    function setSentence(new_sentence) {
        sentence = new_sentence;
        element.innerHTML = sentenceToWord(sentence).map(textToParagraph).reduce((a, b) => a + b, '');
    }

    setSentence(json[randInt(0, json.length - 1)].quote);
}