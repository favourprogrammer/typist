import { toWords, toParagraph } from "../src/sentences.js"

describe('sentences module', () => {
    const quote = "something else we can become";

    it('can convert strings to words', () => {
        expect(toWords(quote)).toEqual(
            ["something ", "else ", "we ", "can ", "become"])
    })

    it('can convert word text to paragraph', () => {
        expect(toParagraph("man")).toBe(
            "<p><span>m</span><span>a</span><span>n</span></p>")
    })
})